Sass Long Shadows

Demo: http://codepen.io/WereHare/pen/poDxK/

USAGE:

.long {
 @include long-text-shadow(30, top right, red, darken(red, 30%));
}

or 

.long {
 @include long-box-shadow(30, top right, red, darken(red, 30%));
}

All arguments optional:

1: length of shadow
2: direction of shadow (currently only supports diagonal directions)
3: starting color of shadow
4: ending color of shadow 
